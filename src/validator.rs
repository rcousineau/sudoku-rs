use crate::board::Board;
use crate::square::Square;
use std::collections::HashMap;

#[derive(Debug)]
pub(crate) struct ValidationError {
    pub(crate) message: String,
}

impl ValidationError {
    fn new(message: String) -> ValidationError {
        ValidationError { message }
    }
}

type Result = std::result::Result<(), ValidationError>;

enum ValidationErrorType {
    DuplicateValue(Square),
    EmptyValue(Square),
}

type ValidationResult = std::result::Result<(), ValidationErrorType>;

pub(crate) fn is_solved(board: &Board) -> Result {
    is_valid(&board, false)
}

pub(crate) fn is_valid(board: &Board, ignore_blank: bool) -> Result {
    for col in 0..9 {
        validate_column(col, &board, ignore_blank)?;
    }

    for row in 0..9 {
        validate_row(row, &board, ignore_blank)?;
    }

    for sector in 0..9 {
        validate_sector(sector, &board, ignore_blank)?;
    }

    Ok(())
}

fn validate_squares(squares: &[&Square], ignore_blank: bool) -> ValidationResult {
    let mut nums = HashMap::new();
    for square in squares {
        match square.value {
            Some(value) => {
                if nums.contains_key(&value) {
                    return Err(ValidationErrorType::DuplicateValue((*square).clone()));
                }
                nums.insert(value, true);
            }
            None => {
                if !ignore_blank {
                    return Err(ValidationErrorType::EmptyValue((*square).clone()));
                }
            }
        }
    }
    Ok(())
}

fn empty_square_error(square: &Square) -> String {
    let (x, y) = square.pos;
    format!("empty value at column {} and row {}", y + 1, x + 1)
}

fn validate_column(col: u8, board: &Board, ignore_blank: bool) -> Result {
    if let Err(error) = validate_squares(&board.column_squares(col), ignore_blank) {
        match error {
            ValidationErrorType::DuplicateValue(square) => {
                return Err(ValidationError::new(format!(
                    "column {} has duplicate of {}",
                    col + 1,
                    square.value.unwrap()
                )));
            }
            ValidationErrorType::EmptyValue(square) => {
                return Err(ValidationError::new(empty_square_error(&square)));
            }
        }
    }
    Ok(())
}

fn validate_row(row: u8, board: &Board, ignore_blank: bool) -> Result {
    if let Err(error) = validate_squares(&board.row_squares(row), ignore_blank) {
        match error {
            ValidationErrorType::DuplicateValue(square) => {
                return Err(ValidationError::new(format!(
                    "row {} has duplicate of {}",
                    row + 1,
                    square.value.unwrap()
                )));
            }
            ValidationErrorType::EmptyValue(square) => {
                return Err(ValidationError::new(empty_square_error(&square)));
            }
        }
    }
    Ok(())
}

fn sector_name(sector: u8) -> &'static str {
    match sector {
        0 => "top left",
        1 => "top middle",
        2 => "top right",
        3 => "left middle",
        4 => "center",
        5 => "right middle",
        6 => "bottom left",
        7 => "bottom middle",
        8 => "bottom right",
        _ => panic!("{} is an invalid sector", sector),
    }
}

fn validate_sector(sector: u8, board: &Board, ignore_blank: bool) -> Result {
    if let Err(error) = validate_squares(&board.sector_squares(sector), ignore_blank) {
        match error {
            ValidationErrorType::DuplicateValue(square) => {
                return Err(ValidationError::new(format!(
                    "{} sector has duplicate of {}",
                    sector_name(sector),
                    square.value.unwrap()
                )));
            }
            ValidationErrorType::EmptyValue(square) => {
                return Err(ValidationError::new(empty_square_error(&square)));
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn load_board(path: &str) -> Board {
        use std::fs;
        use std::str::FromStr;

        let s = fs::read_to_string(path).unwrap();
        Board::from_str(&s).unwrap()
    }

    #[test]
    fn test_validator_blank_board() {
        let b = load_board("examples/blank_board");
        assert!(is_solved(&b).is_err());
    }

    #[test]
    fn test_validator_solved_board() {
        let b = load_board("examples/solved_board");
        assert!(is_solved(&b).is_ok());
    }

    #[test]
    fn test_validator_duplicate_value() {
        let mut b = load_board("examples/solved_board");
        let s = b.square_mut((0, 0)).unwrap();
        s.value = Some(2);
        assert!(is_solved(&b).is_err());
    }
}
