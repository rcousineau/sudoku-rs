use std::str::FromStr;

pub(crate) mod board;
pub(crate) mod solver;
pub(crate) mod square;
pub(crate) mod validator;

pub fn new_game() {
    let b = board::Board::default();
    println!("{}", b);
}

pub fn check_solution(s: &str) {
    let b = board::Board::from_str(s).unwrap();
    if let Err(error) = validator::is_solved(&b) {
        eprintln!("{:?}", error);
    }
}

pub fn solve_board(s: &str) {
    let b = board::Board::from_str(s).unwrap();
    solver::solve(b);
}

pub fn simulate_guessing(s: &str) {
    let b = board::Board::from_str(s).unwrap();
    solver::simulate(b);
}
