use std::fs;
use sudoku;

fn main() {
    let s = fs::read_to_string("examples/blank_board").unwrap();
    sudoku::solve_board(&s);
}
