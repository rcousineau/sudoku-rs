use crate::square::Square;
use regex::Regex;
use std::fmt;
use std::str::FromStr;

#[derive(Clone, Debug)]
pub(crate) struct Board {
    pub(crate) squares: Vec<Square>,
}

impl Board {
    fn new(squares: Vec<Square>) -> Board {
        Board { squares }
    }

    pub(crate) fn square_mut(&mut self, pos: (u8, u8)) -> Option<&mut Square> {
        self.squares.iter_mut().find(|s| s.pos == pos)
    }

    pub(crate) fn blank_squares(&self) -> Vec<&Square> {
        self.squares.iter().filter(|s| s.value.is_none()).collect()
    }

    pub(crate) fn column_squares(&self, col: u8) -> Vec<&Square> {
        self.squares
            .iter()
            .filter(|s| {
                let (x, _) = s.pos;
                col == x
            })
            .collect()
    }

    pub(crate) fn row_squares(&self, row: u8) -> Vec<&Square> {
        self.squares
            .iter()
            .filter(|s| {
                let (_, y) = s.pos;
                row == y
            })
            .collect()
    }

    // sectors:
    //
    // +---+---+---+
    // | 0 | 1 | 2 |
    // +---+---+---+
    // | 3 | 4 | 5 |
    // +---+---+---+
    // | 6 | 7 | 8 |
    // +---+---+---+
    pub(crate) fn sector_squares(&self, sector: u8) -> Vec<&Square> {
        self.squares
            .iter()
            .filter(|s| {
                let (x, y) = s.pos;
                let (x_min, x_max) = match sector % 3 {
                    // sectors 0, 3, and 6
                    0 => (0, 2),
                    // sectors 1, 4, and 7
                    1 => (3, 5),
                    // sectors 2, 5, and 8
                    2 => (6, 8),
                    _ => panic!("{} is an invalid sector", sector),
                };
                let (y_min, y_max) = match sector / 3 {
                    // sectors 0, 1, and 2
                    0 => (0, 2),
                    // sectors 3, 4, and 5
                    1 => (3, 5),
                    // sectors 6, 7, and 8
                    2 => (6, 8),
                    _ => panic!("{} is an invalid sector", sector),
                };
                x >= x_min && x <= x_max && y >= y_min && y <= y_max
            })
            .collect()
    }
}

impl Default for Board {
    fn default() -> Board {
        let mut squares = Vec::new();
        for y in 0..9 {
            for x in 0..9 {
                squares.push(Square::new((x, y), None));
            }
        }
        Board { squares }
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut result = String::new();
        for row in 0..9 {
            if row == 0 || row == 3 || row == 6 {
                result.push_str("+===+===+===+===+===+===+===+===+===+\n");
            } else {
                result.push_str("+---+---+---+---+---+---+---+---+---+\n");
            }
            let s = self.row_squares(row);
            result.push_str(&format!(
                "‖ {} | {} | {} ‖ {} | {} | {} ‖ {} | {} | {} ‖\n",
                &s[0], &s[1], &s[2], &s[3], &s[4], &s[5], &s[6], &s[7], &s[8]
            ));
        }
        result.push_str("+===+===+===+===+===+===+===+===+===+\n");
        write!(f, "{}", result)
    }
}

#[derive(Debug)]
pub struct ParseBoardError;

impl FromStr for Board {
    type Err = ParseBoardError;

    fn from_str(s: &str) -> Result<Board, ParseBoardError> {
        let mut nums = Vec::new();
        let num_pattern = Regex::new(r" ( ) | (\d) ").unwrap();
        for cap in num_pattern.captures_iter(s) {
            if cap.get(1).is_some() {
                nums.push(None);
            }
            if let Some(num) = cap.get(2) {
                nums.push(Some(num.as_str().parse::<u8>().unwrap()))
            }
        }
        let mut squares = Vec::new();
        for (index, num) in nums.iter().enumerate() {
            squares.push(Square::new(((index / 9) as u8, (index % 9) as u8), *num));
        }

        Ok(Board::new(squares))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_board_display() {
        let board = format!("{}", Board::default());
        assert_eq!(
            board,
            concat!(
                "+===+===+===+===+===+===+===+===+===+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+---+---+---+---+---+---+---+---+---+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+---+---+---+---+---+---+---+---+---+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+===+===+===+===+===+===+===+===+===+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+---+---+---+---+---+---+---+---+---+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+---+---+---+---+---+---+---+---+---+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+===+===+===+===+===+===+===+===+===+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+---+---+---+---+---+---+---+---+---+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+---+---+---+---+---+---+---+---+---+\n",
                "‖   |   |   ‖   |   |   ‖   |   |   ‖\n",
                "+===+===+===+===+===+===+===+===+===+\n"
            )
        );
    }
}
