use std::fmt;

#[derive(Clone, Debug)]
pub(crate) struct Square {
    pub(crate) pos: (u8, u8),
    pub(crate) value: Option<u8>,
}

impl Square {
    pub(crate) fn new(pos: (u8, u8), value: Option<u8>) -> Square {
        Square { pos, value }
    }
}

impl fmt::Display for Square {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.value {
            Some(value) => write!(f, "{}", value),
            None => write!(f, " "),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_square_display() {
        let square = Square::new((0, 0), None);
        assert_eq!(&format!("{}", square), " ");

        let square = Square::new((0, 0), Some(3));
        assert_eq!(&format!("{}", square), "3");
    }

    #[test]
    fn test_square_debug() {
        let square = Square::new((0, 0), None);
        assert_eq!(
            &format!("{:?}", square),
            "Square { pos: (0, 0), value: None }"
        );

        let square = Square::new((0, 0), Some(3));
        assert_eq!(
            &format!("{:?}", square),
            "Square { pos: (0, 0), value: Some(3) }"
        );
    }
}
