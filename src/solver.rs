use crate::board::Board;
use crate::validator;
use rand::{thread_rng, Rng};

pub(crate) struct PossibleValue {
    pub(crate) pos: (u8, u8),
    pub(crate) value: u8,
}

impl PossibleValue {
    pub(crate) fn new(pos: (u8, u8), value: u8) -> PossibleValue {
        PossibleValue { pos, value }
    }
}

pub(crate) fn simulate(mut board: Board) {
    loop {
        println!("{}", board);

        let blank_squares = board.blank_squares();
        if blank_squares.is_empty() {
            println!("done");
            break;
        }
        println!("blank squares: {}", blank_squares.len());
        if let Some(PossibleValue { pos, value }) = find_non_guess_square(&board) {
            // this is definitely the value for this square, so place it
            println!("putting {} in position {:?}", value, pos);
            let s = board.square_mut(pos).unwrap();
            s.value = Some(value);
        } else {
            // get random blank square
            let mut rng = thread_rng();
            let i: usize = rng.gen_range(0, blank_squares.len() - 1);
            let pos = blank_squares[i].pos;
            let valid_values = possible_values(pos, board.clone());
            if valid_values.is_empty() {
                eprintln!("board is broken, no valid values for position {:?}", pos);
                break;
            }
            // get random valid value and place it
            let i: usize = rng.gen_range(0, valid_values.len() - 1);
            let value = valid_values[i];
            println!("guessing {} in position {:?}", value, pos);
            let s = board.square_mut(pos).unwrap();
            s.value = Some(value);
        }
    }
}

pub(crate) fn solve(board: Board) {
    let mut attempts = 0;
    loop {
        if attempts > 100 {
            eprintln!("failed after 100 attempts");
            break;
        }
        if try_solve(board.clone()).is_ok() {
            println!("successful after {} attempts", attempts + 1);
            break;
        }
        attempts += 1;
    }
}

fn try_solve(mut board: Board) -> Result<(), ()> {
    loop {
        let blank_squares = board.blank_squares();
        if blank_squares.is_empty() {
            println!("{}", board);
            return Ok(());
        }
        if let Some(PossibleValue { pos, value }) = find_non_guess_square(&board) {
            // this is definitely the value for this square, so place it
            let s = board.square_mut(pos).unwrap();
            s.value = Some(value);
        } else {
            // get random blank square
            let mut rng = thread_rng();
            let i: usize = rng.gen_range(0, blank_squares.len() - 1);
            let pos = blank_squares[i].pos;
            let valid_values = possible_values(pos, board.clone());
            if valid_values.is_empty() {
                return Err(());
            }
            // get random valid value and place it
            let i: usize = rng.gen_range(0, valid_values.len() - 1);
            let value = valid_values[i];
            let s = board.square_mut(pos).unwrap();
            s.value = Some(value);
        }
    }
}

fn find_non_guess_square(board: &Board) -> Option<PossibleValue> {
    for square in board.blank_squares() {
        let valid_values = possible_values(square.pos, board.clone());
        if valid_values.len() == 1 {
            return Some(PossibleValue::new(square.pos, valid_values[0]));
        }
    }
    None
}

fn possible_values(pos: (u8, u8), mut board: Board) -> Vec<u8> {
    (1..=9)
        .filter(|value| {
            let mut s = board.square_mut(pos).unwrap();
            s.value = Some(*value);
            validator::is_valid(&board, true).is_ok()
        })
        .collect()
}
